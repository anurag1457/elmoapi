import tensorflow as tf
import os
from bilm import TokenBatcher, BidirectionalLanguageModel, weight_layers, \
    dump_token_embeddings
from typing import *



class ElmoEmbedder():
    def __init__(self, vocabFile : str):
        self.vocab_file = vocabFile
        print(os.getcwd())
        datadir = os.path.join('tests', 'fixtures', 'model')
        options_file = os.path.join(datadir, 'elmo_2x4096_512_2048cnn_2xhighway_options.json')
        weight_file = os.path.join(datadir, 'elmo_2x4096_512_2048cnn_2xhighway_SQuAD_weights.hdf5')
        token_embedding_file = 'elmo_token_embeddings_squad_final.hdf5'
        dump_token_embeddings(
            self.vocab_file, options_file, weight_file, token_embedding_file
        )
        tf.reset_default_graph()


        self.batcher = TokenBatcher(self.vocab_file)
        self.context_token_ids = tf.placeholder('int32', shape=(None, None))
        self.question_token_ids = tf.placeholder('int32', shape=(None, None))

        self.bilm = BidirectionalLanguageModel(
            options_file,
            weight_file,
            use_character_inputs=False,
            embedding_weight_file=token_embedding_file
        )

        context_embeddings_op = self.bilm(self.context_token_ids)
        question_embeddings_op = self.bilm(self.question_token_ids)

        self.elmo_context_input = weight_layers('input', context_embeddings_op, l2_coef=0.0)
        with tf.variable_scope('', reuse=True):
            # the reuse=True scope reuses weights from the context for the question
            self.elmo_question_input = weight_layers(
                'input', question_embeddings_op, l2_coef=0.0
            )

        self.elmo_context_output = weight_layers(
            'output', context_embeddings_op, l2_coef=0.0
        )
        with tf.variable_scope('', reuse=True):
            # the reuse=True scope reuses weights from the context for the question
            self.elmo_question_output = weight_layers(
                'output', question_embeddings_op, l2_coef=0.0
            )

        self.sess = tf.Session()
        self.sess.run(tf.global_variables_initializer())
        return

    def embed_qapair(self, tok_context : List[List[str]], tok_question : List[List[str]]):
        context_ids = self.batcher.batch_sentences(tok_context)
        question_ids = self.batcher.batch_sentences(tok_question)

        context_emb, question_emb = self.sess.run([self.elmo_context_input['weighted_op'],
                                                    self.elmo_question_input['weighted_op']],
                      feed_dict={self.context_token_ids : context_ids,
                                 self.question_token_ids : question_ids})
        return context_emb, question_emb

    def embed_paragraph(self, tok_context : List[List[str]]):
        context_ids = self.batcher.batch_sentences(tok_context)
        ret = self.sess.run([[self.elmo_context_input['weighted_op']]],
                            feed_dict={self.context_token_ids : context_ids})
        return ret[0]