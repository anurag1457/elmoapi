# elmoapi


from elmo_api import *
#
embedder = ElmoEmbedder('vocab_small.txt')

#To get elmo embeddings of a QA pair
context_vec, question_vec = embedder.embed_qapair(tokenized_context, tokenized_question)

#To get elmo embeddings of a list of sentences
paragraph_vec = embedder.embed_paragraph(tokenized_context)
