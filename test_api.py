from elmo_api import *
import os

raw_context = [
    'He goes to the shop.',
    'Him goes to the shop.'
]
tokenized_context = [sentence.split() for sentence in raw_context]
tokenized_question = [
    ['What', 'are', 'biLMs', 'useful', 'for', '?'],
]

embedder = ElmoEmbedder('vocab_small.txt')
context_vec, question_vec = embedder.embed_qapair(tokenized_context, tokenized_question)
paragraph_vec = embedder.embed_paragraph(tokenized_context)
print()